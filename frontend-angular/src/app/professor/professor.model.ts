export class Professor {
  constructor(
    public firstname: string,
    public lastname: string,
    public email: string,
    public id? : number,  
  ){}
}
