import { Component, OnInit, Input  } from '@angular/core';
//Importar modelo Professor
import {Professor} from '../professor.model';

@Component({
  selector: 'app-professor-detail',
  templateUrl: './professor-detail.component.html',
  styleUrls: ['./professor-detail.component.scss']
})
export class ProfessorDetailComponent implements OnInit {

  //Property Binding
  //Carga el valor del profesor seleccionado en la variable professor
  @Input() professor: Professor;

  constructor() { }

  ngOnInit(): void {
  }

}
