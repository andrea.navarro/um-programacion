import { Component, OnInit } from '@angular/core';
//Importar modelo de professor
import {Professor} from './professor.model';
// Importar servicio
import { ProfessorService } from './professor.service';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.scss']
})
export class ProfessorComponent implements OnInit {
  //Variable que contendrá la lista de profesores
  professors: Professor[]
  //Variable que contendrá el profesor seleccionado
  selectedProfessor: Professor;

  //Importar el servicio en el constructor
  constructor(private professorService: ProfessorService) { }

  ngOnInit(): void {
     //Llamar a la función al inicalizar el componente
     this.getProfessors();
  }

  //Método que obtiene los professores del servicio
  getProfessors(): void {
    //Subscribir la variable professors al resultado obtenido por el servicio
    //de manera asincrónica
    this.professorService.getProfessors()
      .subscribe(professors => this.professors = professors["professors"]);
  }

  //Método que carga el profesor seleccionado a la variable selectedProfessor
  onSelect(professor: Professor): void {
    this.selectedProfessor = professor;
  }

  //Función para eliminar profesores
  delete(professor: Professor): void {
    this.professorService.deleteProfessor(professor.id)
    .subscribe( data => {
      this.getProfessors();
    });
  }

}
