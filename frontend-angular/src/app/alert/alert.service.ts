import { Injectable } from '@angular/core';
import { Alert } from './alert';
import { isUndefined } from 'util';
import { AlertType } from './alert.enum';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  //Lista que contendrá todas las alertas cargadas en la aplicación
  public static Alerts = new Array<Alert>();

  //Método que agrega una Alerta a la lista
  public add(type: AlertType, message: string): void {
    //Crear nueva alerta
    const alert = new Alert();
    //Cargar tipo
    alert.type = type;
    //Cargar mensaje
    alert.message = message;
    //Agregar a la lista
    AlertService.Alerts.push(alert);
  }

  //Eliminar alerta
  public close(alert: Alert): void {
    this.closeIdx(AlertService.Alerts.indexOf(alert));
  }

  //Eliminar alerta de la lista a partir del índice
  private closeIdx(index): void {
    AlertService.Alerts.splice(index, 1);
  }

  //Vaciar lista de alertas
  public clear(): void {
    if (!isUndefined(AlertService.Alerts)) {
    AlertService.Alerts.forEach(alert => {
    this.close(alert);
    });
  }
}

  constructor() { }
}
