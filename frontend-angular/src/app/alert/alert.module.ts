import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent } from './alert/alert.component';
import { AlertBodyComponent } from './alert-body/alert-body.component';
import { AlertService } from './alert.service';

@NgModule({
  declarations: [
    AlertBodyComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [
    AlertBodyComponent,
    AlertComponent
  ],
  providers: [
    AlertService
  ]
})
export class AlertModule { }
