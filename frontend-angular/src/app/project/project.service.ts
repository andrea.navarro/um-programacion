//Importar modelo
import {Project} from './project.model';
//Importar observable
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
//Importar librerías
import {HttpClientModule, HttpClient, HttpErrorResponse} from '@angular/common/http';
//Importar URL de la API
import {API_URL} from '../env';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  //Función que obtiene los proyectos
  getProjects(): Observable<Project[]> {
    //Hacer request a la API
    return this.http.get<Project[]>(API_URL+'/projects')
  }

  getProject(id): Observable<Project> {
    return this.http.get<Project>(API_URL+'/project/'+id);
 }

  //Función que agrega el proyecto
  createProject(project: Project): Observable<Project> {
    return this.http.post<Project>(API_URL+'/projects', project);
  }

  //Función que agrega el proyecto
  editProject(project: Project): Observable<Project> {
    return this.http.put<Project>(API_URL+'/project/'+project.id, project);
  }

  //Función para el manejo de errores HTTP
  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }
}
