import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
//Importar servicio de autenticación
import { AuthService } from './auth.service';
//Importar alerta
import { AlertService } from './alert/alert.service';
import { AlertType } from './alert/alert.enum';

@Injectable({
  providedIn: 'root',

})
export class AuthGuardService implements CanActivate  {

  constructor(public auth: AuthService,
              public router: Router,
              private alert: AlertService,) {}

  //Función que determina si una ruta puede o no navegarse
  canActivate(): boolean {
  //Verificar si el usuario está atuenticado
   if (!this.auth.isAuthenticated) {
     //Si no redireccionar a login
     this.router.navigate(['login']);
     //Cargar alerta
     this.alert.add(AlertType.warning, 'Login to continue');
     //No se puede navegar
     return false;
   }
   //Se puede navegar
   return true;
 }
}
