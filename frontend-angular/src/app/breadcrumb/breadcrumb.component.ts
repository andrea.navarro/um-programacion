import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, Event } from '@angular/router';
import { IBreadCrumb } from './breadcrumb.iterface';
import { filter, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  //Lista de elementos del breadcrumb
  public breadcrumbs: IBreadCrumb[]

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    //Lama a función que construye el Breadcrumb
    this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
   }

  ngOnInit(): void {
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
    })
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    //Obtener etiqueta y ruta de un breadcrumb
    //Verificar que no es null
      let label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.breadcrumb : '';
      let path = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';
            // If the route is dynamic route such as ':id', remove it
      const lastRoutePart = path.split('/').pop();
      //Verificar si la ruta es dinámica (Caso edit que lleva un ID)
      const isDynamicRoute = lastRoutePart.startsWith(':');
      //Si es dinámica cargar la variable dinámica en el breadcrumn
      if(isDynamicRoute && !!route.snapshot) {
        const paramName = lastRoutePart.split(':')[1];
        path = path.replace(lastRoutePart, route.snapshot.params[paramName]);
        label = label+" / "+route.snapshot.params[paramName];
      }

      //Reconstruir cada url para el estado actual
      const nextUrl = path ? `${url}/${path}` : url;

     //Crear objeto breadcrumb
      const breadcrumb: IBreadCrumb = {
          label: label,
          url: nextUrl,
      };

      //Agregar solamente las rutas que tienen breadcrumb asigadas
      const newBreadcrumbs = breadcrumb.label ? [ ...breadcrumbs, breadcrumb ] : [ ...breadcrumbs];
      if (route.firstChild) {
          //Si el breadcrumb asignado no coincide con la ruta actual agregar siguiente breadcrumb
          return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
      }
      return newBreadcrumbs;
  }

}
