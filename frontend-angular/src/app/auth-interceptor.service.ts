import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() { }
  //Función de intercepción que se ejecutará cada vez que se haga una request HTTP
  intercept(req: HttpRequest<any>,
             next: HttpHandler): Observable<HttpEvent<any>> {

         //Obtener token
       const token = localStorage.getItem("token");
       //Si existe el token agregarlo a la request
       if (token) {
           const cloned = req.clone({
               headers: req.headers.set("Authorization",
                   "Bearer " + token)
           });
           //Ejecutar request con la autorización agregada
           return next.handle(cloned);
       }
       else {
          //Ejecutar request
           return next.handle(req);
       }
   }
}
