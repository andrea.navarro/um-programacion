from .. import jwt
from flask import jsonify
from flask_jwt_extended import verify_jwt_in_request, get_jwt_claims
from functools import wraps

def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['role'] =="admin" :
            return fn(*args, **kwargs)
        else:
            return 'Only admins can access', 403
    return wrapper

#Define el atributo que se utilizará para identificar el usuario
@jwt.user_identity_loader
def user_identity_lookup(professor):
    return professor.id

#Define que atributos se guardarán dentro del token
@jwt.user_claims_loader
def add_claims_to_access_token(professor):
    return {'role': professor.role, 'id':professor.id, 'email':professor.email}
