from flask import request, jsonify, Blueprint

from .. import db
from main.models import ProfessorModel, ProjectModel
from flask_jwt_extended import jwt_required, get_jwt_identity, create_access_token
from main.mail.functions import sendMail


auth = Blueprint('auth', __name__, url_prefix='/auth')

@auth.route('/login', methods=['POST'])
def login():
    professor = db.session.query(ProfessorModel).filter(ProfessorModel.email == request.get_json().get("email")).first_or_404()
    if professor.validate_pass(request.get_json().get("password")):
        access_token = create_access_token(identity=professor)
        data ='{"id":"'+str(professor.id)+'","email":"'+str(professor.email)+'", "access_token":"'+access_token+'"}'
        return data, 200
    else:
        return 'Incorrect password', 401

@auth.route('/mail/all', methods=['GET'])
def mailall():
    projects = db.session.query(ProjectModel).all()
    print(str(projects))
    #Obtener solo los mails de los profesores y convertilo a arreglo
    to = [email for email, in db.session.query(ProfessorModel.email)]
    try:
        sent = sendMail(to,"Test mail",'mail/projects',projects = projects)
        if sent == True:
            return "Mail enviado", 200
        else:
            return str(sent), 502
    except Exception as error:
        return str(error), 409

@auth.route('/register', methods=['POST'])
def register():
    professor = ProfessorModel.from_json(request.get_json())
    exists = db.session.query(ProfessorModel).filter(ProfessorModel.email == professor.email).scalar() is not None
    if exists:
        return 'Duplicated mail', 409
    else:
        try:
            db.session.add(professor)
            #sent = sendMail([professor.email],"Register",'mail/register',professor = professor)
            #if sent == True:
            db.session.commit()
            #else:
                #db.session.rollback()
                #return str(sent), 502
        except Exception as error:
            db.session.rollback()
            return str(error), 409
        return professor.to_json() , 201
