from .. import db
from werkzeug.security import generate_password_hash, check_password_hash

class Professor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    projects = db.relationship("Project", back_populates="professor", passive_deletes='all')
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    role = db.Column(db.String(10), nullable=False, default="user")
    @property
    def plain_password(self):
        raise AttributeError('Password cant be read')
    @plain_password.setter
    def plain_password(self, password):
        self.password = generate_password_hash(password)
    def validate_pass(self,password):
        return check_password_hash(self.password, password)
    def __repr__(self):
        return '<Professor: %r %r >' % (self.firstname, self.lastname)
    #Convertir objeto en JSON
    def to_json(self):
        professor_json = {
            'id': self.id,
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
            'email': str(self.email),
            }
        return professor_json

    def to_json_public(self):
        professor_json = {
            'firstname': str(self.firstname),
            'lastname': str(self.lastname),
        }
        return professor_json

    @staticmethod
    #Convertir JSON a objeto
    def from_json(professor_json):
        id = professor_json.get('id')
        firstname = professor_json.get('firstname')
        lastname = professor_json.get('lastname')
        email = professor_json.get('email')
        password = professor_json.get('password')
        role = professor_json.get('role')
        return Professor(id=id,
                    firstname=firstname,
                    lastname=lastname,
                    email=email,
                    plain_password=password,
                    role=role,

                    )
