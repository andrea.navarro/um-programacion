import os
from flask import Flask
from dotenv import load_dotenv
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_mail import Mail
#Importar CORS
from flask_cors import CORS

api = Api()
db = SQLAlchemy()
jwt = JWTManager()
mailsender = Mail()

def create_app():
    app = Flask(__name__)
    load_dotenv()
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    if os.getenv('SQLALCHEMY_DATABASE_TYPE') == 'sqlite':
        #Si no existe el archivo de base de datos crearlo (solo válido si se utiliza SQLite)
        if not os.path.exists(os.getenv('SQLALCHEMY_DATABASE_PATH')+os.getenv('SQLALCHEMY_DATABASE_NAME')+'.db'):
            os.mknod(os.getenv('SQLALCHEMY_DATABASE_PATH')+os.getenv('SQLALCHEMY_DATABASE_NAME')+'.db')
        #Url de configuración de base de datos
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////'+os.getenv('SQLALCHEMY_DATABASE_PATH')+os.getenv('SQLALCHEMY_DATABASE_NAME')+'.db'

    if os.getenv('SQLALCHEMY_DATABASE_TYPE') == 'mysql':
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+os.getenv('SQLALCHEMY_DATABASE_USER')+':'+os.getenv('SQLALCHEMY_DATABASE_PASS')+'@'+os.getenv('SQLALCHEMY_DATABASE_IP')+'/'+os.getenv('SQLALCHEMY_DATABASE_NAME')
    db.init_app(app)

    #Verifica si la conexion es sqlite
    if os.getenv('SQLALCHEMY_DATABASE_TYPE') == 'sqlite':
        def activatePrimaryKeys(conection, conection_record):
            #Ejecuta el comando que activa claves foraneas en sqlite
            conection.execute('pragma foreign_keys=ON')

        with app.app_context():
            from sqlalchemy import event
            #Al conectar a la base de datos llamar a la función que activa la claves foraneas
            event.listen(db.engine, 'connect', activatePrimaryKeys)

    app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET_KEY')
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = int(os.getenv('JWT_ACCESS_TOKEN_EXPIRES'))
    jwt.init_app(app)

    from main.auth import routes
    import main.resources as resources

    api.add_resource(resources.ProfessorsResource, '/professors')
    api.add_resource(resources.ProfessorResource, '/professor/<id>')
    api.add_resource(resources.ProjectsResource, '/projects')
    api.add_resource(resources.ProjectResource, '/project/<id>')
    api.init_app(app)

    app.register_blueprint(auth.routes.auth)

    app.config['MAIL_HOSTNAME'] = os.getenv('MAIL_HOSTNAME')
    app.config['MAIL_SERVER'] = os.getenv('MAIL_SERVER')
    app.config['MAIL_PORT'] = os.getenv('MAIL_PORT')
    app.config['MAIL_USE_TLS'] = os.getenv('MAIL_USE_TLS')
    app.config['MAIL_USERNAME'] = os.getenv('MAIL_USERNAME')
    app.config['MAIL_PASSWORD'] = os.getenv('MAIL_PASSWORD')
    app.config['FLASKY_MAIL_SENDER'] = os.getenv('FLASKY_MAIL_SENDER')
    mailsender.init_app(app)

    #Permitir solicitudes de otros orígenes
    cors = CORS(app, support_credentials=True)
    app.config['CORS_HEADERS'] = 'Content-Type'
    cors = CORS(app, resources={r"*": {"origins": "*"}})

    return app
