from flask_restful import Resource
from flask import request, jsonify, Blueprint
from .. import db, jwt
from main.models import ProfessorModel
from flask_jwt_extended import jwt_required, get_jwt_identity, jwt_optional

#Recurso Profesor
class Professor(Resource):
    #Obtener recurso
    @jwt_required
    def get(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        return professor.to_json()
    #Eliminar recurso
    @jwt_required
    def delete(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        db.session.delete(professor)
        try:
          db.session.commit()
        except Exception as error:
            db.session.rollback()
            return '', 409
        return '', 204
    #Modificar recurso
    @jwt_required
    def put(self, id):
        professor = db.session.query(ProfessorModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            setattr(professor, key, value)
        db.session.add(professor)
        db.session.commit()
        return professor.to_json() , 201

#Recurso Profesores
class Professors(Resource):
    #Obtener lista de recursos
    @jwt_optional
    def get(self):
        professors = db.session.query(ProfessorModel).all()
        return jsonify({ 'professors': [professor.to_json() for professor in professors] })
