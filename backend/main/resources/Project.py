from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.auth.decorators import admin_required
from main.models import ProjectModel, ProfessorModel
from flask_jwt_extended import jwt_required, get_jwt_identity

#Recurso Proyecto
class Project(Resource):
    #Obtener recurso
    @jwt_required
    def get(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        return project.to_json()
    #Eliminar recurso
    @jwt_required
    def delete(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        db.session.delete(project)
        db.session.commit()
        return '', 204
    #Modificar recurso
    @jwt_required
    def put(self, id):
        project = db.session.query(ProjectModel).get_or_404(id)
        data = request.get_json().items()
        for key, value in data:
            if(key != "professor"):
                setattr(project, key, value)
        db.session.add(project)
        db.session.commit()
        return project.to_json() , 201

#Recurso Proyectos
class Projects(Resource):
    #Obtener lista de recursos
    @jwt_required
    def get(self):
        page = 1
        per_page = 50
        projects = db.session.query(ProjectModel)
        if request.get_json():
            filters = request.get_json().items()
            for key, value in filters:
                #Filtros
                #Filtros professorId
                if key == "professorId":
                    projects = projects.filter(ProjectModel.professorId == value)
                if key == "name":
                    projects = projects.filter(ProjectModel.name.like("%"+value+"%"))
                #Filtros year
                if key == "year":
                    projects = projects.filter(ProjectModel.year == value)
                if key == "year[gte]":
                    projects = projects.filter(ProjectModel.year >= value)
                if key == "year[lte]":
                    projects = projects.filter(ProjectModel.year <= value)
                #Ordenamiento
                if key =="sort_by":
                    #Ordenamiento por name
                    if value == "name":
                        projects = projects.order_by(ProjectModel.name)
                    if value == "name.desc":
                        projects = projects.order_by(ProjectModel.name.desc())
                    #Ordenamiento por lastname
                    if value == "professor":
                        projects = projects.join(ProjectModel.professor).order_by(ProfessorModel.lastname)
                    if value == "professor.desc":
                        projects = projects.join(ProjectModel.professor).order_by(ProfessorModel.lastname.desc())
                    #Ordenamiento por year
                    if value == "year":
                        projects = projects.order_by(ProjectModel.year)
                    if value == "year.desc":
                        projects = projects.order_by(ProjectModel.year.desc())

                #Paginación
                if key =="page":
                    page = int(value)
                if key == "per_page":
                    per_page = int(value)

        projects = projects.paginate(page, per_page, True, 30)
        #Devolver además de los datos la cantidad de páginas y elementos existentes antes de paginar
        return jsonify({ 'projects': [project.to_json() for project in projects.items],
                  'total': projects.total,
                  'pages': projects.pages,
                  'page': page
                  })
    #Insertar recurso
    @jwt_required
    def post(self):
        project = ProjectModel.from_json(request.get_json())
        #current_user = get_jwt_identity()
        try:
            db.session.add(project)
            db.session.commit()
        except:
            return '', 400
        return project.to_json(), 201
