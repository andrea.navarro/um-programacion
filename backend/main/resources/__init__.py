from .Professor import Professor as ProfessorResource
from .Professor import Professors as ProfessorsResource

from .Project import Project as ProjectResource
from .Project import Projects as ProjectsResource
