# - *- coding: utf- 8 - *-
from flask_wtf import FlaskForm #Importa funciones de formulario
from wtforms import PasswordField, SubmitField, StringField #Importa campos
from wtforms.fields.html5 import EmailField #Importa campos HTML
from wtforms import validators #Importa validaciones

class ProfessorForm(FlaskForm):

    #Definición de campo String
    firstname = StringField('Firstname',
    [
        #Definición de validaciones
        validators.Required(message = "Firstname is required")
    ])

    lastname = StringField('Lastname',
    [
        validators.Required(message = "Lastname is required")
    ])

    #Definición de campo de mail
    email = EmailField('E-mail',
    [
        validators.Required(message = "E-mail is require"),
        validators.Email( message ='Formad not valid'),
    ])

    #Definición de campo de contraseña
    password = PasswordField('Password', [
        validators.Required(),
         #El campo de contraseña debe coincidir con el de confirmuar
        validators.EqualTo('confirm', message='Passwords dont match')
    ])

    confirm = PasswordField('Repeat Password')

    #Definición de campo submit
    submit = SubmitField("Send")
