# - *- coding: utf- 8 - *-
from flask_wtf import FlaskForm #Importa funciones de formulario
from wtforms import PasswordField, SubmitField, StringField, SelectField, HiddenField#Importa campos
from wtforms.fields.html5 import EmailField, DateTimeField #Importa campos HTML
from wtforms import validators #Importa validaciones

class ProjectFilterForm(FlaskForm):

    name = StringField('Name', [validators.optional()])
    yearFrom = DateTimeField('From year', [validators.optional()],format='%Y',)
    yearTo = DateTimeField('To year', [validators.optional()],format='%Y',)
    professorId = SelectField('Professor',[validators.optional()], coerce=int,)
    submit = SubmitField("Filter")
    download = SubmitField("Download")

class ProjectForm(FlaskForm):

    #Definición de campo String
    name = StringField('Name',
    [
        #Definición de validaciones
        validators.Required(message = "Name is required")
    ])

    year = DateTimeField('Year', format='%Y')

    professorId = SelectField('Professor', coerce=int)
    #Definición de campo submit
    submit = SubmitField("Send")
