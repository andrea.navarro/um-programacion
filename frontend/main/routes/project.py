from flask import Blueprint, render_template, current_app, redirect, url_for, request
from ..forms.project_form import ProjectForm
import requests, json
from flask_login import login_required, LoginManager
from ..forms.project_form import ProjectFilterForm
from .auth import admin_required
import io, csv
from flask import make_response

project = Blueprint('project', __name__, url_prefix='/project')

@project.route('/')
@login_required
def index():
    #Eliminar la protección csrf para el formulario de filtro
    #Cargar parametros de la url en el formulario
    filter = ProjectFilterForm(request.args,meta={'csrf': False})
    #Obtener profesores
    r = requests.get(
            current_app.config["API_URL"]+'/professors',
            headers={"content-type":"application/json"})
    #Cargar profesores en formulario
    professors = [(item['id'], item['lastname']+" "+item['firstname']) for item in json.loads(r.text)["professors"]]
    filter.professorId.choices = professors
    filter.professorId.choices.insert(0, [0,"All"])
    #Obtener token
    auth = request.cookies['access_token']
    #Crear headers
    headers = {
    'content-type': "application/json",
    'authorization': "Bearer "+auth
    }
    data = {}
    #Aplicado de filtros
    #Validar formulario
    if filter.validate() :
        if filter.yearFrom.data and filter.yearTo.data:
            if filter.yearFrom.data == filter.yearTo.data:
                data["year"] = filter.yearTo.data.year
        if filter.yearFrom.data != None:
            data["year[gte]"] = filter.yearFrom.data.year
        if filter.yearTo.data != None:
            data["year[lte]"] = filter.yearTo.data.year
        if filter.name.data != None:
            data["name"] = filter.name.data
        if filter.professorId.data != None and filter.professorId.data != 0:
            data["professorId"] = filter.professorId.data

    #Ordenamiento
    if 'sort_by' in request.args:
        data["sort_by"] = request.args.get('sort_by','')

    #Si se quiere descargar el archivo
    if 'download' in request.args:
        if request.args.get('download','') == 'Download':
            code = 200
            #Comenzar por la primera página
            page = 1
            list_projects = []
            #Recorrer hasta que no haya más páginas
            while code == 200:
                data['page'] = page
                #Llamada a la api
                r = requests.get(
                    current_app.config["API_URL"]+'/projects',
                    headers = headers,
                    data = json.dumps(data))
                code = r.status_code
                if(code==200):
                    #Recorrer los projectos de la página y colocar los campos que se quieren agregar
                    for project in json.loads(r.text)["projects"]:
                        element = {
                            'name': project["name"],
                            'professor': project["professor"]["firstname"]+" "+project["professor"]["lastname"],
                            'year': project["year"]
                        }
                        #Agregar cada elemento a la lista
                        list_projects.append(element)
                #Aumentar en uno el número de página
                page += 1

            #Inicializar para poder escribir en el buffer de memoria
            si = io.StringIO()
            #Inicializar el objeto que va a escribir el csv a partir de un diccionario
            #Pasar las claves del diccionario como cabecera
            fc = csv.DictWriter(si, fieldnames=list_projects[0].keys(),)
            #Escribir la cabecera
            fc.writeheader()
            #Escribir las filas
            fc.writerows(list_projects)

            #Crear una respuesta que tiene como contenido el valor del buffer
            output = make_response(si.getvalue())
            #Colocar cabeceras para que se descargue como un archivo
            output.headers["Content-Disposition"] = "attachment; filename=projects.csv"
            output.headers["Content-type"] = "text/csv"
            #Devolver salida
            return output

    #Número de página
    if 'page' in request.args:
        data["page"] = request.args.get('page','')
    else:
        if 'page' in data:
            del data["page"]

    #Obtener datos de la api para la tabla
    r = requests.get(
        current_app.config["API_URL"]+'/projects',
        headers = headers,
        data = json.dumps(data))

    if(r.status_code==200):
        #Cargar proyectos
        projects = json.loads(r.text)["projects"]
        #Cargar datos de paginación
        pagination = {}
        pagination["total"] = json.loads(r.text)["total"]
        pagination["pages"] = json.loads(r.text)["pages"]
        pagination["current_page"] = json.loads(r.text)["page"]
        title = "Project - List"
        return render_template('project_list.html', title=title, projects=projects, filter=filter, pagination = pagination  ) #Mostrar template
    else:
        return redirect(url_for('main.logout'))

@project.route('/view/<int:id>')
@login_required
def view(id):
    r = requests.get(current_app.config["API_URL"]+'/project/'+str(id),headers={"content-type":"application/json"})
    if(r.status_code==404):
        return redirect(url_for('project.index'))
    project = json.loads(r.text)
    title = "Project - View"
    return render_template('project_view.html', title=title, project=project  ) #Mostrar template

@project.route('/create', methods=["POST","GET"])
@login_required
@admin_required
def create():
    form = ProjectForm() #Instanciar formulario
    r = requests.get(
            current_app.config["API_URL"]+'/professors',
            headers={"content-type":"application/json"})
    professors = [(item['id'], item['lastname']+" "+item['firstname']) for item in json.loads(r.text)["professors"]]
    form.professorId.choices = professors
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        auth = request.cookies['access_token']
        headers = {
        'content-type': "application/json",
        'authorization': "Bearer "+auth
        }
        data = {}
        data["name"] = form.name.data
        data["year"] = form.year.data.year
        data["professorId"] = form.professorId.data
        r = requests.post(
            current_app.config["API_URL"]+'/projects',
            headers = headers,
            data = json.dumps(data))
        if(r.status_code==201):
            return redirect(url_for('project.index')) #Redirecciona a lista
    return render_template('project_form.html', form = form) #Muestra el formulario
