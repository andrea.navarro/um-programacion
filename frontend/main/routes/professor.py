from flask import Blueprint, render_template, current_app, redirect, url_for
from ..forms.professor_form import ProfessorForm
from ..forms.auth_forms import  LoginForm
import requests, json

professor = Blueprint('professor', __name__, url_prefix='/professor')

@professor.route('/')
def index():
    r = requests.get(current_app.config["API_URL"]+'/professors',headers={"content-type":"application/json"})
    professors = json.loads(r.text)["professors"]
    title = "Professor - List"
    loginForm = LoginForm()
    return render_template('professor_list.html', title=title, professors=professors, loginForm = loginForm ) #Mostrar template

@professor.route('/view/<int:id>')
def view(id):
    r = requests.get(
        current_app.config["API_URL"]+'/professor/'+str(id),
        headers={"content-type":"application/json"})
    if(r.status_code==404):
        return redirect(url_for('professor.index'))
    professor = json.loads(r.text)
    title = "Professor - View"
    return render_template('professor_view.html', title=title, professor=professor  ) #Mostrar template

@professor.route('/create', methods=["POST","GET"])
def create():
    form = ProfessorForm() #Instanciar formulario
    if form.validate_on_submit(): #Si el formulario ha sido enviado y es validado correctamente
        return redirect(url_for('professor.index')) #Redirecciona a lista
    return render_template('professor_form.html', form = form) #Muestra el formulario
