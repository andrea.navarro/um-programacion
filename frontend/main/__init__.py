import os
from flask import Flask, flash, redirect, url_for
from dotenv import load_dotenv
from flask_wtf import CSRFProtect #importar para proteccion CSRF
from flask_login import LoginManager

login_manager = LoginManager()
csrf = CSRFProtect()

def create_app():
    app = Flask(__name__)
    load_dotenv()
    csrf.init_app(app)
    app.config['API_URL'] = os.getenv('API_URL')
    from main.routes import main, professor, project, auth
    login_manager.init_app(app)
    app.config["SECRET_KEY"] = os.getenv('SECRET_KEY')
    app.register_blueprint(routes.main.main)
    app.register_blueprint(routes.professor.professor)
    app.register_blueprint(routes.project.project)
    return app
